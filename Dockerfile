FROM centos:7

ENV PGPRO_VERSION  10.3.2-1
ENV PGPRO_REPO_RPM http://repo.postgrespro.ru/pgpro-10/keys/postgrespro-std-10.centos.pro.yum-1.0-1.noarch.rpm

ENV PGDATA /pgdata

ENV GOSU_VERSION 1.10
ENV LANG en_US.utf8

RUN rpm -ivh ${PGPRO_REPO_RPM} && \
    yum install -y \
      wget \
      postgrespro-std-10-server-${PGPRO_VERSION}.el7.centos \
      postgrespro-std-10-client-${PGPRO_VERSION}.el7.centos \
      postgrespro-std-10-libs-${PGPRO_VERSION}.el7.centos \
      postgrespro-std-10-contrib-${PGPRO_VERSION}.el7.centos && \
    wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-amd64" && \
    chmod +x /usr/local/bin/gosu && \
    mkdir /docker-entrypoint-initdb.d && \
    mkdir /docker-entrypoint-init.d && \
    mkdir -p ${PGDATA} && \
    chown postgres -R ${PGDATA}

ENV PATH /opt/pgpro/std-10/bin:$PATH

COPY docker-entrypoint.sh /

VOLUME /pgdata

EXPOSE 5432

ENTRYPOINT [ "/docker-entrypoint.sh" ]
CMD [ "postgres" ]
